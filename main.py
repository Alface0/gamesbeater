#!/usr/bin/env python
import os
import sys
import curses

import pickle
from pybrain.rl.agents import LearningAgent
from pybrain.rl.experiments import Experiment
from pybrain.rl.learners import ActionValueNetwork, NFQ

from ale_environment import ArcadeEnvironment
from ale_task import ArcadeTask

if len(sys.argv) < 3:
    print("Usage:", sys.argv[0], "rom_file", "run_type", "network_file")
    sys.exit()

# Train or Test
if sys.argv[2] == "TRAIN":
    TRAIN_RUN = True
elif sys.argv[2] == "TEST":
    TRAIN_RUN = False
else:
    print("Wrong run type given! The options are TRAIN or TEST")
    sys.exit()

if TRAIN_RUN:
    DISPLAY_SCREEN = False
    KEYBOARD_QUIT = True
else:
    DISPLAY_SCREEN = True
    KEYBOARD_QUIT = False

if KEYBOARD_QUIT:
    screen = curses.initscr()
    curses.noecho()
    curses.cbreak()
    screen.keypad(True)
    screen.nodelay(True)

TRAINING_ITERATIONS = 100
MAX_STEPS_WITHOUT_SCORING = 100000000000000

# Initialize Environment
environment = ArcadeEnvironment(display_screen=DISPLAY_SCREEN)
environment.initialize_game(sys.argv[1])

# Initialize agent
controller = None
if len(sys.argv) == 4:
    if os.path.isfile(sys.argv[3]):
        decision_network = open(sys.argv[3], "rb")
        controller = pickle.load(decision_network)

if controller is None:
    controller = ActionValueNetwork(environment.width * environment.height, len(environment.all_actions))

# Create Agent
learner = NFQ()
agent = LearningAgent(controller, learner)

# Create task associated with the environment
task = ArcadeTask(environment)

# Connect the task and the agent in the experiment
experiment = Experiment(task, agent)

game_number = 0
is_finished = False

# Learn when the result is better than the average
total_score = 0
scores_count = 0
while not is_finished:
    print("------ Training --------")
    agent.reset()

    if game_number != 0:
        environment.reset()
        task.reset()

    if TRAIN_RUN:
        experiment.doInteractions(TRAINING_ITERATIONS)
        agent.learn()

        if KEYBOARD_QUIT:
            key = screen.getch()
            if key == ord('q'):
                print("Training is stopping...")
                is_finished = True
                break

    if not TRAIN_RUN:
        print("------ Testing ------")
        iterations = 0
        while not environment.get_ale().game_over():
            experiment._oneInteraction()
            iterations += 1

        average_score = 0
        exact_average = 0.0
        if game_number != 0:  # There is no average before the first play
            exact_average = float(total_score) / float(scores_count)
            average_score = int(exact_average)

        total_score += task.getScore()
        scores_count += 1

        print("----- Results -----")
        print("Iterations", iterations)
        print("Score", task.getScore())
        print("Exact Average", exact_average)
        print("Average", average_score)

        if task.getScore() > 30:
            is_finished = True

        game_number += 1

if TRAIN_RUN:
    print("SAVING THE DECISIONS NETWORK")

    decisions_network = open("decisions_network.pkl", "wb")
    pickle.dump(controller, decisions_network)
    decisions_network.close()

    print("FINISHED!")

    if KEYBOARD_QUIT:
        curses.nocbreak(); screen.keypad(0); curses.echo()
        curses.endwin()
