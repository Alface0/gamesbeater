from pybrain.rl.environments import Task


class ArcadeTask(Task):
    """
        This task will only work with the ArcadeEnvironment
    """

    def __init__(self, environment):
        super(ArcadeTask, self).__init__(environment)
        self.last_reward = None
        self.score = 0

        # Pre-compute the positions of each group of pixels to not need to make the calculations every time
        self.position_groups = []

        # Group the monitor pixels positions in groups of 4x4
        pixel_y = 0
        """
        while pixel_y < self.env.height:
            pixel_x = 0

            while pixel_x < self.env.width:
                # Create tuple with the current group positions
                group_positions = []
                for i in range(self.env.group_height):
                    for j in range(self.env.group_width):
                        group_positions.append((pixel_y + i) * self.env.width + pixel_x + j)

                self.position_groups.append(group_positions)

                pixel_x += self.env.group_width
            pixel_y += self.env.group_height
        """

    def getReward(self):
        if self.last_reward > 0:
            return self.score
        else:
            return 0

    def getScore(self):
        return self.score

    def performAction(self, action):
        """ The action vector is stripped and the only element is cast to integer and given
            to the super class.
        """
        self.last_reward = self.env.perform_action(action)
        self.score += self.last_reward

    def getObservation(self):
        """
            :return: The current state of the screen
        """

        # Aggregate pixels by groups in order to reduce the needed inputs in the network
        """
        screen_data = self.env.get_screen_data()
        grouped_data = []

        for position_group in self.position_groups:
            grouped_data.append(sum([screen_data[position] for position in position_group]) / len(position_group))
        """
        return self.env.get_ale().getRAM()

    def reset(self):
        self.last_reward = None
        self.score = 0
