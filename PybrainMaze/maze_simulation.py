from scipy import *
import pickle

from pybrain.rl.environments.mazes import Maze, MDPMazeTask
from pybrain.rl.learners.valuebased import ActionValueTable
from pybrain.rl.agents import LearningAgent
from pybrain.rl.learners import Q, SARSA
from pybrain.rl.experiments import Experiment
from pybrain.rl.environments import Task

import time

SUCCESSES_TO_FINISH = 5


def clear_terminal():
    print("\x1b[2J\x1b[H")

decision_table = open("best_decisions_table.pkl", "rb")
controller = pickle.load(decision_table)

structure = array([[1, 1, 1, 1, 1, 1, 1, 1, 1],
                   [1, 0, 0, 1, 0, 0, 0, 0, 1],
                   [1, 0, 0, 1, 0, 0, 1, 0, 1],
                   [1, 0, 0, 1, 0, 0, 1, 0, 1],
                   [1, 0, 0, 1, 0, 1, 1, 0, 1],
                   [1, 0, 0, 0, 0, 0, 1, 0, 1],
                   [1, 1, 1, 1, 1, 1, 1, 0, 1],
                   [1, 0, 0, 0, 0, 0, 0, 0, 1],
                   [1, 1, 1, 1, 1, 1, 1, 1, 1]])

environment = Maze(structure, (7, 7))

learner = Q()
agent = LearningAgent(controller, learner)

task = MDPMazeTask(environment)

experiment = Experiment(task, agent)

average = 0
successes_count = 0
while successes_count < SUCCESSES_TO_FINISH:
    experiment.doEpisodes(100)
    agent.learn(1)
    i = 0

    reached_goal = False
    while not reached_goal:
        i += 1

        if experiment._oneInteraction() >= 1:
            reached_goal = True
            average += i
            successes_count += 1
            break

        print(experiment.task.env)
        print("\n")
        print("Current Position:", experiment.task.getObservation())

        time.sleep(0.5)
        clear_terminal()

    print("Reached the goal in", i, "iterations")
    time.sleep(2)

print("An average of " + str(average / SUCCESSES_TO_FINISH) + " were needed to solve each maze")
