from numpy import argmax
from pybrain.rl.learners import ActionValueTable


class MyActionValueTable(ActionValueTable):
    def __init__(self, numStates, numActions):
        super(MyActionValueTable, self).__init__(numStates, numActions)

    def _forwardImplementation(self, inbuf, outbuf):
        """ Take a vector of length 1 (the state coordinate) and return
            the action with the maximum value over all actions for this state.
        """

        # Map the decision table for the actions in each possible state
        decision_table = self.params.reshape(self.numRows, self.numColumns)

        # Select the weights for the actions in the current state
        state_weights = decision_table[inbuf[0], :]

        # In the case the weights are in different rows pack them in 1 dimension
        packed_state_weights = state_weights.flatten()

        # Select the index(Action) with the maximum weight in the current state
        outbuf[0] = argmax(packed_state_weights)
