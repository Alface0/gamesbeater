from pybrain.rl.agents.logging import LoggingAgent
from scipy import *
import sys, time
import pickle

from pybrain.rl.environments.mazes import Maze, MDPMazeTask
from pybrain.rl.learners.valuebased import ActionValueTable
from pybrain.rl.agents import LearningAgent
from pybrain.rl.learners import Q, SARSA
from pybrain.rl.experiments import Experiment
from pybrain.rl.environments import Task

import time

from my_interface import MyActionValueTable

TRAINING_ITERATIONS = 10000
MAX_SIMULATION_ITERATIONS = 100000
ITERATIONS_GOAL = 20
SUCCESSES_TO_FINISH = 30


def clear_terminal():
    print("\x1b[2J\x1b[H")

structure = array([[1, 1, 1, 1, 1, 1, 1, 1, 1],
                   [1, 0, 0, 1, 0, 0, 0, 0, 1],
                   [1, 0, 0, 1, 0, 0, 1, 0, 1],
                   [1, 0, 0, 1, 0, 0, 1, 0, 1],
                   [1, 0, 0, 1, 0, 1, 1, 0, 1],
                   [1, 0, 0, 0, 0, 0, 1, 0, 1],
                   [1, 1, 1, 1, 1, 1, 1, 0, 1],
                   [1, 0, 0, 0, 0, 0, 0, 0, 1],
                   [1, 1, 1, 1, 1, 1, 1, 1, 1]])

environment = Maze(structure, (7, 7))

controller = MyActionValueTable(81, 4)
controller.initialize(1.)

learner = Q()
agent = LearningAgent(controller, learner)

task = MDPMazeTask(environment)

experiment = Experiment(task, agent)

i = 0
while i < SUCCESSES_TO_FINISH:
    # Train TRAINING_ITERATIONS
    agent.reset()
    experiment.doInteractions(TRAINING_ITERATIONS)
    agent.learn()

    # Test the capabilities to reach the goal by checking the number of iterations needed
    reached_goal = False
    iterations = 0
    while not reached_goal:
        iterations += 1

        obs = task.getObservation()
        agent.integrateObservation(obs)

        LoggingAgent.getAction(agent)
        agent.lastaction = agent.module.activate(agent.lastobs)

        if agent.learning:
            agent.lastaction = agent.learner.explore(agent.lastobs, agent.lastaction)

        task.performAction(agent.lastaction)

        reward = task.getReward()
        agent.giveReward(reward)

        if reward >= 1:
            print("Reached Goal in " + str(iterations) + " iterations")
            reached_goal = True

            if iterations < ITERATIONS_GOAL:
                end_training = True
                i += 1

        if iterations > MAX_SIMULATION_ITERATIONS:
            print("Max iterations reached, aborted simulation")
            i = 0
            break

print(SUCCESSES_TO_FINISH, "simulations from random position done with success")

decisions_table = open("decisions_table.pkl", "wb")
pickle.dump(controller, decisions_table)
decisions_table.close()

print("Saved the agent decision table in decisions_table.pkl")
