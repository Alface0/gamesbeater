import random

from ale_python_interface import ALEInterface
from pybrain.rl.environments.environment import Environment
import numpy as np
import ctypes


class ArcadeEnvironment(Environment):
    # Actions
    all_actions = []

    # Current state
    perseus = None

    # Initial Score
    score = 0

    def __init__(self, sound=False, display_screen=False):
        # Define variables
        self.height = None
        self.width = None
        self.screen_data = None

        # Initialize Arcade Learning Environment
        self.ale = ALEInterface()
        self.ale.setBool(ctypes.c_char_p(b'sound'), ctypes.c_bool(sound))
        self.ale.setBool(ctypes.c_char_p(b'display_screen'), ctypes.c_bool(display_screen))

        # Define the length of the groups of pixels
        self.group_width = None
        self.group_height = None

        self.rom_name = None

    def perform_action(self, action):
        return self.ale.act(action)

    def initialize_game(self, rom_name):
        self.rom_name = rom_name

        # Initialize Game
        self.ale.setInt(ctypes.c_char_p(b"random_seed"), ctypes.c_int(random.randint(0, 1000)))
        self.ale.loadROM(ctypes.c_char_p(rom_name.encode()))

        # Register game specific data
        self.width, self.height = len(self.ale.getRAM()), 1
        self.screen_data = np.zeros(self.width * self.height, dtype=np.uint8)
        self.all_actions = self.ale.getLegalActionSet()

        # Define the length of the groups of pixels
        self.group_width = 6
        self.group_height = 6

        # Check the possible size of the groups by width and height
        while self.width % self.group_width != 0:
            self.group_width -= 1

        while self.height % self.group_height != 0:
            self.group_height -= 1

        print(self.group_width)
        print(self.group_height)

    def get_ale(self):
        return self.ale

    def get_screen_data(self):
        # Make sure the game is initialized
        assert self.screen_data is not None

        self.screen_data = self.ale.getScreen(screen_data=self.screen_data)
        return self.screen_data

    def getSensors(self):
        obs = np.zeros(len(self.all_actions))

        return obs

    def reset(self):
        self.perseus = None

        # Re-initialize Game
        self.ale.setInt(ctypes.c_char_p(b"random_seed"), ctypes.c_int(random.randint(0, 1000)))
        self.ale.reset_game()
