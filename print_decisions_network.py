import sys
import pickle

decision_network = open(sys.argv[1], "rb")
controller = pickle.load(decision_network)

# Print the weights for each connection between the network layers
for module in controller.network.modules:
    for connection in controller.network.connections[module]:
        print connection
        for i in range(len(connection.params)):
            print connection.whichBuffers(i), connection.params[i]

decision_network.close()
