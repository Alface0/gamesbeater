# GamesBeater - ALE + Pybrain2
This project integrates the pybrain2 reinforcement learning implementations with the Arcade Learning Environment to allow the creation of bots which can play atari 2600 games.

## Dependencies

* Install pybrain 

```sh
git clone https://github.com/drmargarido/pybrain.git
cd pybrain
sudo python setup.py install
```

* Install Arcade Learning Environment

```sh
wget https://github.com/mgbellemare/Arcade-Learning-Environment/archive/v0.6.0.tar.gz
tar -xf v0.6.0.tar.gz
cd Arcade-Learning-Environment-0.6.0
mkdir build && cd build
cmake -DUSE_SDL=ON -DUSE_RLGLUE=OFF -DBUILD_EXAMPLES=ON ..
make -j 4
cd ..
sudo pip install .
```


## Run a simulation

* Run a training simulation

```sh
python main.py <game_name> TRAIN 
```

* Run a training simulation with a base trained bot as starting point

```sh
python main.py <game_name> TRAIN <network_name>
```

* Run a test simulation

```sh
python main.py <game_name> TEST
```

* Run a test simulation with a already trained bot

```sh
python main.py <game_name> TEST <network_name>
```


## Screenshots

![Bot Playing Breakout](/screenshots/breakout.png)
![Bot Playing Enduro](/screenshots/enduro.png)

## Sources

* [Pybrain](http://www.pybrain.org/) used for taking advantage of their already implemented reinforced learning algorithms

* [Arcade Learning Environment](http://www.arcadelearningenvironment.org/) used as a benchmark platform since it allow us to run atari2600 games and interact with the game.

* M. G. Bellemare, Y. Naddaf, J. Veness, and M. Bowling, “The arcade learning environment: An evaluation platform for general agents,” Journal of Artificial Intelligence Research, p. 253–279, 2013.

* M. Hausknecht, J. Lehman, R. Miikkulainen, and P. Stone, “A neuroevolution
approach to general atari game playing,” IEEE Transactions on Computational Intelligence and AI in Games, p. 1–13, 2013.

* X. Guo, S. Singh, H. Lee, R. Lewis, and X. Wang, “Deep learning for real-time atari game play using offline monte-carlo tree search planning,” NIPS 2014, 2014.

* J. Togelius, S. Karakovskiy, J. Koutnik, and J. Schmidhuber, “Super mario evolution,” Proceedings of IEEE Symposium on Computational Intelligence and Games (CIG), 2009.
